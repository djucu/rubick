**DESCRIEREA GENERALĂ A APLICAȚIEI**

![](media/718c23cd5df0adccbd9afeda6f60ca31.jpg)

Aplicația realizată se numește Makeup Planner, si reprezintă o agendă digitală
destinată make-up artiștilor care are scopul de a-i ajuta în organizarea rapidă
și eficientă a programărilor, precum și a ideilor, sau a oricăror alte subiecte
legate de această temă.

![](media/3d337ea69299a4b5b2f4aada842445d3.jpg)

![](media/2652c6df75425a26ee194cd833faf8cd.jpg)

Această aplicație este împărțită în două părți principale: calendarul în care se
pot adăuga programările, și notițele, unde se pot adăuga orice texte dorite de
utilizator.

![](media/7de524ebc628a2cb8463f7f54c16e8b7.jpg)
  
Programările conțin informații precum numele persoanei programate, data
programării, intervalul orar, tipul de machiaj, precum și obervații. Odată
adăugate,

![](media/aba1d32ea4eca3d5f0736d27ff2a663d.jpg)

![](media/cd69012527855da81bcfc127e98792ad.jpg)

aplicația permite utilizatorului să modifice sau chiar să șteargă programările.

Numărul maxim de notițe care se pot adăuga este opt. Odată ce se depășește acest
număr, se vor adăuga notițele noi, însă se vor șterge, pe rând, primele
adăugate.

![](media/fcd21372093a3c104484220ae97bb7b2.jpg)

![](media/bc26a9a8de118dd7cb095b1ff92d2262.jpg)

**IMPLEMENTARE ȘI FUNCȚIONALITATE**

Implementarea aplicației s-a realizat prin programare în cod sursă cu limbajele
PHP, JavaScript, CSS si HTML, având la bază metode de programare precum clase,
liste și arbori. Stocarea datelor se face prin fișiere.

![](media/6c51e523fba1b1073f4f9e00a5e3dcef.jpg)

![](media/a40097dac0dac6a1758134885761c56c.jpg)

Fiind o aplicație web, folosește un server Apache (implementarea și testarea s-a
realizat cu ajutorul aplicației XAMPP).

Proiectul a fost realizat și testat pe un laptop cu procesor Intel Core I7,
2.90GHz, 16GB RAM, sistem de opererare Windows 10 Professional 64 bit. Pentru
rulare și testare, au fost folosite browserele Google Chrome, Mozilla Firefox și
Internet Explorer.
